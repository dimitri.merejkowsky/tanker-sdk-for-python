.. image:: https://img.shields.io/badge/License-Apache%202.0-blue.svg
  :target: https://opensource.org/licenses/Apache-2.0
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
  :target: https://github.com/ambv/black

Tanker Python SDK
=================

Overview
--------

The `Tanker SDK <https://tanker.io>`_ provides an easy-to-use SDK allowing you to protect your users'
data.

This repository only contains Python3 bindings. The core library can be found in the `TankerHQ/sdk-native GitHub project <https://github.com/TankerHQ/sdk-native>`_.

Contributing
------------

We are actively working to allow external developers to build and test this project
from source. That being said, we welcome feedback of any kind. Feel free to
open issues on the GitHub bug tracker.

Documentation
-------------

See `API documentation <https://tankerhq.github.io/sdk-python>`_.

A more high-level documentation is being written. In the mean time, feel free
to read `the documentation about the JavaScript SDK
<https://tanker.io/docs/latest/?language=javascript/>`_.
Most of it is applicable to the Python SDK too.
